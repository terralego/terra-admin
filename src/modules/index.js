import * as CRUD from './CRUD';
import * as RA from './RA';

export default { CRUD, RA };
