import { withTranslation } from 'react-i18next';
import CategorySelector from './CategorySelector';

export default withTranslation()(CategorySelector);
