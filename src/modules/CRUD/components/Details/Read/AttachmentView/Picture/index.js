import { withTranslation } from 'react-i18next';
import Picture from './Picture';

export default withTranslation()(Picture);
